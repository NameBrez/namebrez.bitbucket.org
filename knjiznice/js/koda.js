
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

var BruceEHR = "";
var JohnEHR = "";
var BarbEHR = "";

var BruceProbs="";
var JohnProbs="";
var BarbProbs="";

var BruceEHRid=""
var JohnEHRid=""
var BarbEHRid=""

var BruceAlerg="";
var JohnAlerg="";
var BarbAlerg="";


function generirajPredlogo(){
    sessionId = getSessionId();
    $.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
    
    //Bruce Wayne
    $.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: "Bruce",
		            lastNames: "Wayne",
		            gender: "MALE",
		            dateOfBirth: "1970-02-19",
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		    
		    BruceEHR=ehrId;
		        
		    $.ajax({
		        url: baseUrl + "/demographics/party",
		        type: 'POST',
		        contentType: 'application/json',
		        data: JSON.stringify(partyData),
	        });
	        
	        //dodamo še nekaj podatkov o njihovem zdravju
	   
        	var telesnaVisina = 188
        	var telesnaTeza = 95
        	var telesnaTemperatura = 36
        	var sistolicniKrvniTlak = 80
        	var diastolicniKrvniTlak = 130
        	var nasicenostKrviSKisikom = 98
        	
        	var podatki={
        	    "ctx/language": "en",
    		    "ctx/territory": "SI",
    		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
    		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
    		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
    		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
    		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
    		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
    		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
        	};
	       
	       BruceProbs =[
        	    "Nothing."
        	   ];
        	   
        	BruceAlerg=[
        		"Cowardice",
        		"Bad jokes",
        		"Kittens"
        		]
	       
	       
	       var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		   };
	       
	      $.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		})
	       
	    }
	});
		
	//John Constantine
    $.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: "John",
		            lastNames: "Constantine",
		            gender: "MALE",
		            dateOfBirth: "1970-10-05",
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        
		        JohnEHR=ehrId;
		        
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		        });
		        
		        //dodamo še nekaj podatkov o njihovem zdravju
	   
        	var telesnaVisina = 182
        	var telesnaTeza = 83
        	var telesnaTemperatura = 38
        	var sistolicniKrvniTlak = 70
        	var diastolicniKrvniTlak = 115
        	var nasicenostKrviSKisikom = 90
        	
        	var podatki={
        	    "ctx/language": "en",
    		    "ctx/territory": "SI",
    		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
    		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
    		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
    		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
    		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
    		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
    		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
        	};
	       
	       JohnProbs =[
        	    "Smokes too much",
        	    "Drinks too much",
        	    "Low blood preasure"
        	   ];
        	   
        	JohnAlerg =[
        		"Cheap cigars",
        		"Swamp herbs",
        		"Lactose"
        		]
	       
	       var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		   };
	       
	        $.ajax({
		        url: baseUrl + "/composition?" + $.param(parametriZahteve),
		        type: 'POST',
		        contentType: 'application/json',
		        data: JSON.stringify(podatki),
		    })
		        
		        
		        
		    }
		});
		
	//Barbara Gordon
    $.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: "Barbara",
		            lastNames: "Wayne",
		            gender: "FEMALE",
		            dateOfBirth: "2004-03-03",
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        
		        BarbEHR=ehrId;
		        
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		        });
		        
		        		        //dodamo še nekaj podatkov o njihovem zdravju
	   
        	var telesnaVisina = 165
        	var telesnaTeza = 56
        	var telesnaTemperatura = 40
        	var sistolicniKrvniTlak = 90
        	var diastolicniKrvniTlak = 140
        	var nasicenostKrviSKisikom = 94
        	
        	var podatki={
        	    "ctx/language": "en",
    		    "ctx/territory": "SI",
    		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
    		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
    		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
    		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
    		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
    		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
    		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
        	};
        	
        	BarbProbs =[
        	    "Fractured tibia",
        	    "High fever",
        	    "High blood preasure"
        	   ];
        	
        	BarbAlerg =[
        		"Gluten",
        		"Flower pollen",
        		"Bees"
        		]
        
	       
	       var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		   };
	       
	        $.ajax({
		        url: baseUrl + "/composition?" + $.param(parametriZahteve),
		        type: 'POST',
		        contentType: 'application/json',
		        data: JSON.stringify(podatki),
		    })
		        
		        
		        
		        
		    }
		});
		
}

function getBruce(){
    sessionId = getSessionId();
    
    $.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
	
	
    $('#predstavitev').html("");
    $('#predstavitev').append("\
        <img id='profilPic' src='https://avatarfiles.alphacoders.com/442/44272.jpg' class='img-rounded' style='width:100px; float:left; margin-top:20px;'>\
            <div style='float:left; margin-left: 10px;'>\
                <h3><span id='ime'> Bruce </span> <span id='priimek'> Wayne</span></h3>\
                <p><b>Gender:</b><span id='spol'> MALE</span></p>\
                <p><b>Date of birth:</b><span id='DOB'> 19th February 1970 </span></p>\
                <p><b>City:</b><span id='city'> Gotham</span></p>\
                <p><b>Ocuppation:</b><span> Not... Batman</span></p>\
                <p><b>EhrID: </b><span>"+ BruceEHR +"</span></p>\
            </div>\  ");
            
            
    $('#diagram').html("");
    $('#diagram').append("Meritve niso bile opravljene.")
     
    var dis= "66%";
    var sis= "88%"
    
    document.getElementById("diastolicni").style.width=dis;
    document.getElementById("sistolicni").style.width=sis;
    $('#bp').html("");
    $('#bp').append("<h3> 130/80 </h3>");
    
    var bmi= 31;
    $('#BMI').html("");
    $('#BMI').append("<h3>"+bmi+"</h3>")
    
    $('#teza').html("");
    $('#teza').append("<h3>95</h3>")
    
    $('#visina').html("");
    $('#visina').append("<h3>188</h3>")
    
    $('#list-problemi').html("");
    for(i in BruceProbs){
    	$('#list-problemi').append("<li>" + BruceProbs[i] + "</li>")
    }
    
    $('#list-alergije').html("");
    for(i in BruceAlerg){
    	$('#list-alergije').append("<li>" + BruceAlerg[i] + "</li>")
    }
    
    $('#doktor').html("")
    $('#doktor').append("<h4> Alfred Pennyworth (also his butler) </h4>")
    
}

function getJohn(){
    sessionId = getSessionId();
    
    $.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
	
    $('#predstavitev').html("");
    $('#predstavitev').append("\
        <img id='profilPic' src='https://media.licdn.com/mpr/mpr/shrinknp_200_200/p/5/005/091/013/0f49750.jpg' class='img-rounded' style='width:100px; float:left; margin-top:20px;'>\
            <div style='float:left; margin-left: 10px;'>\
                <h3><span id='ime'> John </span> <span id='priimek'> Constantine</span></h3>\
                <p><b>Gender:</b><span id='spol'> MALE</span></p>\
                <p><b>Date of birth:</b><span id='DOB'> unknown </span></p>\
                <p><b>City:</b><span id='city'> unknown</span></p>\
                <p><b>Ocuppation:</b><span> Excorcist</span></p>\
                <p><b>EhrID: </b><span> "+JohnEHR+" </span></p>\
            </div>\  ");
            
    var dis= "60%";
    var sis= "76%"
    
    $('#diagram').html("");
    $('#diagram').append("Meritve niso bile opravljene.")
    
    document.getElementById("diastolicni").style.width=dis;
    document.getElementById("sistolicni").style.width=sis;
    $('#bp').html("");
    $('#bp').append("<h3> 115/70 </h3>");
    
    var bmi= 27;
    $('#BMI').html("");
    $('#BMI').append("<h3>"+bmi+"</h3>")
    
    $('#teza').html("");
    $('#teza').append("<h3>82</h3>")
    
    $('#visina').html("");
    $('#visina').append("<h3>182</h3>")
    
    $('#list-problemi').html("");
    for(i in JohnProbs){
    	$('#list-problemi').append("<li>" + JohnProbs[i] + "</li>")
    }
    
    $('#list-alergije').html("");
    for(i in JohnAlerg){
    	$('#list-alergije').append("<li>" + JohnAlerg[i] + "</li>")
    }
    
    $('#doktor').html("")
    $('#doktor').append("<h4> doctor Chas Chandler </h4>")
}

function getBarb(){
    sessionId = getSessionId();
    
    $.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
	
	
    $('#predstavitev').html("");
    $('#predstavitev').append("\
        <img id='profilPic' src='http://img13.deviantart.net/b29d/i/2014/192/f/d/batgirl_by_toonikun-d7q6ixg.jpg' class='img-rounded' style='width:100px; float:left; margin-top:20px;'>\
            <div style='float:left; margin-left: 10px;'>\
                <h3><span id='ime'> Barbara </span> <span id='priimek'> Gordon</span></h3>\
                <p><b>Gender:</b><span id='spol'> FEMALE</span></p>\
                <p><b>Date of birth:</b><span id='DOB'> 3rd March 2003 </span></p>\
                <p><b>City:</b><span id='city'> Gotham</span></p>\
                <p><b>EhrID: </b><span> "+BarbEHR+" </span></p>\
            </div>\  ");
            
            
            
    var dis= "75%";
    var sis= "93%"
    
    
    $('#diagram').html("");
    $('#diagram').append("Meritve niso bile opravljene.")
    
    document.getElementById("diastolicni").style.width=dis;
    document.getElementById("sistolicni").style.width=sis;
    
    $('#bp').html("");
    $('#bp').append("<h3> 140/90 </h3>");
    
    var bmi= 18;
    $('#BMI').html("");
    $('#BMI').append("<h3>"+bmi+"</h3>")
    
    $('#teza').html("");
    $('#teza').append("<h3>56</h3>")
    
    $('#visina').html("");
    $('#visina').append("<h3>165</h3>")
    
    $('#list-problemi').html("");
    for(i in BarbProbs){
    	$('#list-problemi').append("<li>" + BarbProbs[i] + "</li>")
    }
    
    $('#list-alergije').html("");
    for(i in BarbAlerg){
    	$('#list-alergije').append("<li>" + BarbAlerg[i] + "</li>")
    }
    
    $('#doktor').html("")
    $('#doktor').append("<h4> doctor Cassandra Cain </h4>")
    
}


function getByEHR(){
	var EHR=$("#ehrVsebina").val();
	var visina;
	var teza;
	sessionId = getSessionId();
    
    $.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
	
	var ime="Prišlo je do napake, poiskusite ponovno."
	var priimek=""
	var spol="?"
	var dateOfBirth="?"
	var city="?"
	
	
	$.ajax({
		url:baseUrl+"/demographics/ehr/"+EHR+"/party",
		type: 'GET',
		success: function(data){
			var party=data.party;
			ime=party.firstNames;
			priimek=party.lastNames;
			spol=party.gender;
			dateOfBirth=party.dateOfBirth;
			
			
			
			$('#predstavitev').html("");
		    $('#predstavitev').append("\
		        <img id='profilPic' src='http://s3.amazonaws.com/37assets/svn/765-default-avatar.png' class='img-rounded' style='width:100px; float:left; margin-top:20px;'>\
	            <div style='float:left; margin-left: 10px;'>\
	                <h3><span id='ime'> "+ime+" </span> <span id='priimek'> "+priimek+"</span></h3>\
	                <p><b>Gender:</b><span id='spol'> "+spol+"</span></p>\
	                <p><b>Date of birth:</b><span id='DOB'> "+ dateOfBirth +" </span></p>\
	                <p><b>City:</b><span id='city'> "+city+"</span></p>\
	                <p><b>EhrID: </b><span> "+ EHR +" </span></p>\
	            </div>\  ");
	        $('#doktor').html("");
	        $('#doktor').append("<h4> doctor Kržišnik </h4>");
		}
	});
	
	//alergije
	$.ajax({
		url:baseUrl+"/view/"+EHR+"/allergy",
		type: 'GET',
		success: function(data){
			$('#list-alergije').html("");
			for(i in data){
				$('#list-alergije').append("<li> "+ data[i].agent +" </li>")
			}
		}
	})
	
	//problemi
	$.ajax({
			url:baseUrl+"/view/"+EHR+"/problem",
			type: 'GET',
			success: function(data){
				$('#list-problemi').html("");
				for(i in data){
					$('#list-problemi').append("<li> "+ data[i].diagnosis +" </li>")
				}
		}
	})
	
	//krvni tlak
	$.ajax({
			url:baseUrl+"/view/"+EHR+"/blood_pressure",
			type: 'GET',
			success: function(data){
				var sis=(data[0].systolic/150)*100
					sis=sis.toFixed();
				var dis=(data[0].diastolic/120)*100
					dis=dis.toFixed();
					
					
					
					
				document.getElementById("diastolicni").style.width=dis+"%";
    			document.getElementById("sistolicni").style.width=sis+"%";	
    			
    			
				$('#bp').html("");
				$('#bp').append("<h3>"+data[0].systolic+"/"+data[0].diastolic+"</h3>");
				

		}
	})
	
	//visina
	$.ajax({
			url:baseUrl+"/view/"+EHR+"/height",
			type: 'GET',
			success: function(data){
				visina=data[0].height;
				$('#visina').html("");
    			$('#visina').append("<h3><span id='he'>"+visina+"</span></h3>")
    			visina = ($('#he').text())
		}
	})
	
	//teza
	$.ajax({
			url:baseUrl+"/view/"+EHR+"/weight",
			type: 'GET',
			success: function(data){
				teza=data[0].weight;
				$('#teza').html("");
    			$('#teza').append("<h3><span id='we'>"+teza+"</span></h3>")
    			teza = ($('#we').text());
    			
    			visina=visina/100
    			console.log(visina)
    			console.log(teza)

    			var bmi= teza/(visina*visina);
    			bmi=bmi.toFixed();
    			
    			$('#BMI').html("");
				$('#BMI').append("<h3>"+bmi+"</h3>");
		}
	});

	var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	$.ajax({
    url: baseUrl + "/view/" + EHR + "/body_temperature",
    type: 'GET',
    headers: {
        "Ehr-Session": sessionId
    },
    success: function (res) {
    	$('#diagram').html("");

        res.forEach(function(el, i, arr) {
            var date = new Date(el.time);
            el.date = date.getDate() + '-' + monthNames[date.getMonth()];
        });

        new Morris.Bar({
            element: 'diagram',
            data: res.reverse(),
            xkey: 'date',
            ykeys: ['temperature'],
            labels: ['Body Temperature'],
            hideHover: true,
            barColors: ['#FFCE54'],
            xLabelMargin: 5,
            resize: true
        });

    }
});
	
}




$(document).ready(function(){
	generirajPredlogo();
})